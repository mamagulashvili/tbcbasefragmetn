package com.example.tbcbasefragment

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.bumptech.glide.load.HttpException
import com.example.tbcbasefragment.model.Data
import java.net.HttpRetryException

class Source : PagingSource<Int, Data>() {

    private val mainRepository: MainRepository by lazy { MainRepository() }

    override fun getRefreshKey(state: PagingState<Int, Data>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Data> {
        try {
            val currentPage = params.key ?: 1
            val response = mainRepository.getUser(currentPage)
            val responseData = mutableListOf<Data>()
            val data = response.data?.data ?: emptyList()
            responseData.addAll(data)
            return LoadResult.Page(
                data = responseData,
                prevKey = if (currentPage == 1) null else currentPage - 1,
                nextKey = if (data.isEmpty()) null else currentPage + 1
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }


}