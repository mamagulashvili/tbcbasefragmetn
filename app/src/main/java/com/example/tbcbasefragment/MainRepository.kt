package com.example.tbcbasefragment

import com.example.tbcbasefragment.api.RetrofitService
import com.example.tbcbasefragment.response.UsersResponse

class MainRepository {

    private val responseHandler: ResponseHandler by lazy { ResponseHandler() }
    suspend fun getUser(page: Int): Resource<UsersResponse> {
        val usersResponse = RetrofitService.userApi.getUser(page)
        try {
            if (usersResponse.isSuccessful) {
                usersResponse.body()?.let {
                    return responseHandler.handleSuccess(it)
                }
                return responseHandler.handleDefaultException()
            }else{
                return responseHandler.handleDefaultException()
            }
        } catch (e: Exception) {
            return responseHandler.handleException(e)
        }
    }
}