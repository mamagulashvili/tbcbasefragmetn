package com.example.tbcbasefragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcbasefragment.databinding.RowItemBinding
import com.example.tbcbasefragment.model.Data

class UsersAdapter : PagingDataAdapter<Data, UsersAdapter.UsersViewHolder>(Differ) {

    object Differ : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.onBind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        return UsersViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class UsersViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var user: Data
        fun onBind() {
            user = getItem(absoluteAdapterPosition)!!
            binding.apply {
                tvName.text = user.firstName
                tvLastName.text = user.lastName
                tvEmailAddress.text = user.email
                Glide.with(ivProfile.context).load(user.avatar).into(ivProfile)
            }
        }
    }
}