package com.example.tbcbasefragment.response

data class Support(
    val text: String,
    val url: String
)