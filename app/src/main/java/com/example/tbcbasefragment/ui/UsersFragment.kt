package com.example.tbcbasefragment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcbasefragment.BaseFragment
import com.example.tbcbasefragment.adapter.UsersAdapter
import com.example.tbcbasefragment.databinding.UsersFragmentBinding

class UsersFragment : BaseFragment<UsersFragmentBinding, UsersViewModel>(
    UsersFragmentBinding::inflate,
    UsersViewModel::class.java
) {
    private val usersAdapter: UsersAdapter by lazy { UsersAdapter() }
    override fun startView(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }


    private fun init() {
        setRecycle()
        observe()
    }

    private fun observe() {
        viewModel.users.observe(viewLifecycleOwner, {
            usersAdapter.submitData(
                lifecycle, it
            )
        })
    }
    private fun setRecycle(){
        binding.rvUser.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = usersAdapter
        }
    }

}