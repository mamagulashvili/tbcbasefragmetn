package com.example.tbcbasefragment.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.tbcbasefragment.Source

class UsersViewModel : ViewModel() {

    var users= Pager(PagingConfig(6)){
        Source()
    }.liveData.cachedIn(viewModelScope)
}