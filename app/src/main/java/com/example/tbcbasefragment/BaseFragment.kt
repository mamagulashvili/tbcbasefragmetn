package com.example.tbcbasefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding, VM : ViewModel>(
    private val inflate: Inflate<VB>,
    vmClass: Class<VM>
) :
    Fragment() {
    var _binding: VB? = null
    val binding get() = _binding!!

    val viewModel: VM by lazy {
        ViewModelProvider(this).get(vmClass)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = inflate.invoke(layoutInflater, container, false)
            startView(layoutInflater, container)
        }

        return binding.root
    }

    abstract fun startView(inflater: LayoutInflater, container: ViewGroup?)
}