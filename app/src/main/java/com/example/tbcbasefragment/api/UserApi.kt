package com.example.tbcbasefragment.api

import com.example.tbcbasefragment.response.UsersResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {
    @GET("users")
    suspend fun getUser(
        @Query("page")
        page: Int
    ): Response<UsersResponse>
}